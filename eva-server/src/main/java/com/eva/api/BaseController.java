package com.eva.api;

import com.eva.core.model.LoginUserInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller基类
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
@Slf4j
public class BaseController {

    /**
     * 获取当前登录用户
     */
    protected LoginUserInfo getLoginUser () {
        return (LoginUserInfo)SecurityUtils.getSubject().getPrincipal();
    }

    /**
     * 获取ID集合
     *
     * @param ids 使用","隔开的多个ID
     * @return List<Integer>
     */
    protected List<Integer> getIdList (String ids) {
        String [] idArray = ids.split(",");
        List<Integer> idList = new ArrayList<>();
        for (String id : idArray) {
            idList.add(Integer.valueOf(id));
        }
        return idList;
    }

    /**
     * 获取文件字节流
     *
     * @param is 输入流
     * @return ByteArrayOutputStream
     */
    protected ByteArrayOutputStream getOutputStream (InputStream is) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] bs = new byte[is.available()];
        int len;
        while ((len = is.read(bs)) != -1) {
            baos.write(bs, 0, len);
        }
        return baos;
    }
}
