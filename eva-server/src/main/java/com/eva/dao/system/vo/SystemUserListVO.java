package com.eva.dao.system.vo;

import com.eva.dao.system.model.SystemRole;
import com.eva.dao.system.model.SystemUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
@Data
@ApiModel("系统用户列表视图对象")
public class SystemUserListVO extends SystemUser {

    @ApiModelProperty(value = "角色")
    private List<SystemRole> roles;

    @ApiModelProperty(value = "创建人信息")
    private SystemUser createUserInfo;

    @ApiModelProperty(value = "更新人信息")
    private SystemUser updateUserInfo;

}
