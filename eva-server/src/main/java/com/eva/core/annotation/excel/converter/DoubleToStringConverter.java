package com.eva.core.annotation.excel.converter;

import com.eva.core.annotation.excel.ExcelDataConverterAdapter;

import java.math.BigDecimal;

/**
 * 小数转字符串
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
public class DoubleToStringConverter implements ExcelDataConverterAdapter {

    /**
     * 转换
     *
     * @param args 参数集合
     * - args[0] 单元格数据
     * - args[1] 保留精度位数
     */
    @Override
    public Object convert(Object... args) {
        Object value = args[0];
        if (value == null) {
            return null;
        }
        BigDecimal decimalValue = new BigDecimal(String.valueOf((value)));
        // 精度处理
        if (args.length > 1) {
            decimalValue = decimalValue.setScale(Integer.valueOf(args[1].toString()), BigDecimal.ROUND_HALF_UP);
        }
        decimalValue.toString();
        return decimalValue.toPlainString();
    }
}
